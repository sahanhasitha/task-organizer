<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $fillable = ['email', 'user_name', 'password', 'country'];

    /**
     * Edit user
     * @param $details
     */
    public static function edit( $details )
    {
        /**
         * get user object
         */
        $user = User::find($details['id']);

        /**
         * set attributes of user
         */
        $user->user_name = $details['user_name'];
        $user->email = $details['email'];
        $user->country = $details['country'];
        $user->password = $details['password'];

        /**
         * edit and save user
         */
        $user->save();
    }

    /**
     * Enable user
     *
     * @param $id
     */
    public static function enable($id)
    {
        /**
         * find user object
         */
        $user = User::find($id);

        /**
         * change user status
         */
        $user->status = true;

        /**
         * edit and save user
         */
        $user->save();
    }

    /**
     * Disable user
     *
     * @param $id
     */
    public static function disable($id)
    {
        /**
         * find user object
         */
        $user = User::find($id);

        /**
         * change user status
         */
        $user->status = false;

        /**
         * edit and save user
         */
        $user->save();
    }

    public function tasks(){
        return $this->belongsToMany('App\Task');
    }
}
