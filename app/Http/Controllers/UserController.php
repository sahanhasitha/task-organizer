<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserUpdateRequest;
use App\TaskUser;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller {

    /**
     * Register user
     * @param UserRegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register( UserRegisterRequest $request )
    {
        $user_details = $request->all();

        User::create([
            'user_name' => $user_details['user_name'],
            'email' => $user_details['email'],
            'country' => $user_details['country'],
            'password' => $user_details['password'],
        ]);

        return redirect('home')->with('msg-success', 'You have registered successfully with our site');
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);

        return view('user.edit', compact('user'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UserUpdateRequest $request
     * @internal param int $id
     * @return Response
     */
	public function update(UserUpdateRequest $request)
	{
		User::edit($request->all());

        return redirect('home')->with('msg-success', 'User updated successful');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);

        return redirect('home')->with('msg-success', 'User deleted successful');
    }

	/**
	 * enable the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function enable($id)
	{
		User::enable($id);

        return redirect('home')->with('msg-success', 'User enabled successful');
    }

	/**
	 * disable the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function disable($id)
	{
		User::disable($id);

        return redirect('home')->with('msg-success', 'User disabled successful');
    }

    /**
     * Assign to task
     * @param $task_id
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assign( $user_id, $task_id )
    {
        TaskUser::create([
            'task_id' => $task_id,
            'user_id' => $user_id
        ]);

        return redirect('home')->with('msg-success', 'Task assigned successful to user');

    }

}
