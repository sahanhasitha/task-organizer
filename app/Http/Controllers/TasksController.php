<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateTaskRequest;
use App\Task;
use Illuminate\Http\Request;

class TasksController extends Controller {

    /**
     * Create task
     * @param CreateTaskRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create( CreateTaskRequest $request ){

        $task = $request->all();

        Task::create(['title' => $task['title']]);

        return redirect('home')->with('msg-success', 'Task created successful');
    }

}
