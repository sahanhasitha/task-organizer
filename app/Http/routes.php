<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::get('auth/register', function(){
    return view('auth/register');
});

Route::post('user/register', 'UserController@register');

Route::get('user/edit/{id}', 'UserController@edit');

Route::post('user/update', 'UserController@update');

Route::get('user/delete/{id}', 'UserController@destroy');

Route::get('user/disable/{id}', 'UserController@disable');

Route::get('user/enable/{id}', 'UserController@enable');

Route::get('task/create', function(){
    return view('tasks.create');
});

Route::post('task/create', 'TasksController@create');

Route::get('user/assign/{user_id}/{task_id}', 'UserController@assign');