@extends('app')

@section('content')
<div class="container">
	<div class="row">
        <a href="{{url('auth/register')}}" class="btn btn-success">Register User</a>
        <a href="{{url('task/create')}}" class="btn btn-success">Add Task</a>
        <hr>
        <div class="col-md-10 col-md-offset-1">
            @if(Session::has('msg-success'))
            <div class="alert alert-success">
                <strong>Success!</strong> <br>
                {{(Session::get('msg-success'))}}
            </div>
            @endif
            <h1>Users</h1>
			<table class="table">
                <td><strong>User name</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>Country</strong></td>
                <td><strong>Change</strong></td>
                <td><strong>Assign</strong></td>
                <td><strong>Assigned Tasks</strong></td>
                <tr>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->user_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->country}}</td>
                            <td>
                                <a class="btn btn-info" href="user/edit/{{$user->id}}">Edit</a>
                                <a class="btn btn-danger"  href="user/delete/{{$user->id}}">Delete</a>
                                @if($user->status)
                                <a class="btn btn-info"  href="user/disable/{{$user->id}}">Deactivate</a>
                                @else
                                <a class="btn btn-info"  href="user/enable/{{$user->id}}">Activate</a>
                                @endif
                            </td>
                            <td>
                                <select id="sel_task_id">
                                    @foreach($tasks as $task)
                                        <option value="{{$task->id}}">{{$task->title}}</option>
                                    @endforeach
                                </select>
                                <button onclick="assignTask({{$user->id}})" class="btn btn-default">Save</button>
                            </td>
                            <td>
                                @foreach($user->tasks()->get() as $task)
                                    {{$task->title}}<br>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tr>
			</table>
		</div>

        <div class="col-md-10 col-md-offset-1">
			<h1>All Tasks</h1>
            <table class="table">
                <td><strong>ID</strong></td>
                <td><strong>Title</strong></td>
                <tr>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{$task->id}}</td>
                            <td>{{$task->title}}</td>
                        </tr>
                    @endforeach
                </tr>
			</table>
		</div>


	</div>
</div>

<script>
    function assignTask(user_id)
    {
        var task_id = document.getElementById('sel_task_id').value;
        location.href = '/user/assign/'+ user_id + '/' + task_id;
    }
</script>
@endsection
